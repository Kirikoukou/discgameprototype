using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BulletGenerator : MonoBehaviour
{
    public Rigidbody Bullet;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Vector3 Dir = ray.direction;
            /*Vector3 mousePosFar = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.farClipPlane);
            Vector3 mousePosF = Camera.main.ScreenToWorldPoint(mousePosFar);
            Vector3 Dir = mousePosF - transform.position; */
            Rigidbody Bullet1 = Instantiate(Bullet,transform.position,Quaternion.identity);
            Bullet1.AddForce(Dir.normalized*10000);
            Debug.Log(Dir);
        }
    }
}
